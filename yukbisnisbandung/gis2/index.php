<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <title>GIS Application - Extended Template by Cyber313</title>
        <meta name="author" content="luckynvic@gmail.com">
        
        

        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <link href="ext/customScroll/css/jquery.mCustomScrollbar.css" rel="stylesheet">
        <link href="css/style.default.css" rel="stylesheet">
<script src="http://maps.google.com/maps?file=api&amp;v=3&amp;sensor=true&amp;key=AIzaSyABAiRMExl_KVCugrFbUO5FJwNTo_94vt0" type="text/javascript"></script>    
</head>
<?php
 include("lib_func.php");
?>
      
    <body onLoad="load()" onUnload="GUnload()">

        <nav class="navbar navbar-default nav-fixed-top" role="navigation" id="app-nav-bar" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <?php judul(); ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <?php menu(); ?>
            </div><!-- /.navbar-collapse -->
        </nav>

        <div id="map-canvas" ></div>
        <div class="visible-lg visible-md">
            <?php test(); 
                  ?>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="ext/customScroll/js/jquery.mCustomScrollbar.min.js"></script>
        <script src="ext/customScroll/js/jquery.mousewheel.min.js"></script>
        <script src="js/application.js"></script>
        <script src="js/kec.js" type="text/javascript"></script>
        <script type="text/javascript">
       
       
 
    var iconBlue = new GIcon(); 
    iconBlue.image = 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
    iconBlue.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconBlue.iconSize = new GSize(12, 20);
    iconBlue.shadowSize = new GSize(22, 20);
    iconBlue.iconAnchor = new GPoint(6, 20);
    iconBlue.infoWindowAnchor = new GPoint(5, 1);

    var iconRed = new GIcon(); 
    iconRed.image = 'http://labs.google.com/ridefinder/images/mm_20_red.png';
    iconRed.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconRed.iconSize = new GSize(12, 20);
    iconRed.shadowSize = new GSize(22, 20);
    iconRed.iconAnchor = new GPoint(6, 20);
    iconRed.infoWindowAnchor = new GPoint(5, 1);

    var customIcons = [];
    customIcons["wisata"] = iconBlue;
    customIcons["museum"] = iconRed;


    function load() {					
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(-6.914263, 107.635640), 13);       
		
        GDownloadUrl("koneksi/koneksi.php", function(data) {
          var xml = GXml.parse(data);
          var markers = xml.documentElement.getElementsByTagName("marker");
          for (var i = 0; i < markers.length; i++) {
            var nama = markers[i].getAttribute("nama");
            var alamat = markers[i].getAttribute("alamat");
            var tipe = markers[i].getAttribute("tipe");
            var point = new GLatLng(parseFloat(markers[i].getAttribute("lat")),
                                    parseFloat(markers[i].getAttribute("lng")));
            var marker = createMarker(point, nama, alamat, tipe);
            map.addOverlay(marker);
          }
        });
      }
    }

    function createMarker(point, nama, alamat, tipe) {
      var marker = new GMarker(point, customIcons[tipe]);
      var html = "<b>" + nama + "</b> <br/>" + alamat;
      GEvent.addListener(marker, 'click', function() {
        marker.openInfoWindowHtml(html);
      });
      return marker;
    }
    //]]>
  </script>
  <div style="position: absolute;  left: 290px;  top: 98px; z-index: 3;" >
    <div id="map" style="width: 1050px; height: 550px"></div>  </div>
    </body>
</html>
