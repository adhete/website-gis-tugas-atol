<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<?php
 
     // Parsing Karakter-Karakter Khusus
     function parseToXML($htmlStr)
     {
          $xmlStr=str_replace('<','<',$htmlStr);
          $xmlStr=str_replace('>','>',$xmlStr);
          $xmlStr=str_replace('"','"',$xmlStr);
          $xmlStr=str_replace("'",'"',$xmlStr);
          $xmlStr=str_replace("&",'&',$xmlStr);
          return $xmlStr;
     }

  // Menghubungkan Koneksi dengan server MySQL
     $host = "localhost";
	 $username="root";
     $password="";
     $database="map";
	 
	 $connection=mysql_connect($host, $username, $password);
    
     if (!$connection) {
          die('Not connected : ' . mysql_error());
          }
     // Memilih database MySQL yang aktif
     $db_selected = mysql_select_db($database, $connection);
     if (!$db_selected) {
          die ('Can\'t use db : ' . mysql_error());
          }
 
     // Memilih semua baris pada tabel 'markers3'
     $query = "SELECT * FROM markers WHERE 1";
     $result = mysql_query($query);
     if (!$result) {
          die('Invalid query: ' . mysql_error());
          }
 
     // Header File XML
     header("Content-type: text/xml");
 
     // Parent node XML
     echo '<markers>';
 
     // Iterasi baris, masing-masing menghasilkan node-node XML
     while ($row = @mysql_fetch_assoc($result)){
 
          // Menambahkan ke node dokumen XML
          echo '<marker ';
          echo 'nama="' . parseToXML($row['nama']) . '" ';
          echo 'alamat="' . parseToXML($row['alamat']) . '" ';
          echo 'lat="' . $row['lat'] . '" ';
          echo 'lng="' . $row['lng'] . '" ';
          echo 'tipe="' . $row['tipe'] . '" ';
          echo '/>';
     }
 
     // Akhir File XML (tag penutup node)
     echo '</markers>';
     
     
     
	}
 
?>
</body>
</html>
