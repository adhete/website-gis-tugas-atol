<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Breakeven by TEMPLATED</title>

<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
<script src="js.js" type="text/javascript"></script>
</head>
<body>

<h1>Dropy</h1>
<h2>A Simple SCSS & jQuery dropdown</h2>

<main>
  <p><a href="http://codepen.io/Tombek/pen/OPvpLe" target="_blank">I've made a new version of Dropy !</a></p>
</main>

<dl class="dropdown">
	<dt><a><span>Dropdown n�1</span></a></dt>
		<dd>
			<ul>
				<li><a class="default">Dropdown n�1</a></li>
				<li><a>Option n�1</a></li>
				<li><a>Option n�2</a></li>
				<li><a>Option n�3</a></li>
			</ul>
		</dd>
</dl>

<dl class="dropdown">
	<dt><a><span>Dropdown n�2</span></a></dt>
		<dd>
			<ul>
				<li><a class="default">Dropdown n�2</a></li>
				<li><a>Option n�1</a></li>
				<li><a>Option n�2</a></li>
				<li><a>Option n�3</a></li>
				<li><a>Option n�4</a></li>
				<li><a>Option n�5</a></li>
				<li><a>Option n�6</a></li>
			</ul>
		</dd>
</dl>

<dl class="dropdown">
	<dt><a><span>Dropdown n�3</span></a></dt>
		<dd>
			<ul>
				<li><a class="default">Dropdown n�3</a></li>
				<li><a>Option n�1</a></li>
				<li><a>Option n�2</a></li>
			</ul>
		</dd>
</dl>

</body>
</html>