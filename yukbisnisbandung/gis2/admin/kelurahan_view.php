<html>

<head>
	<?php include("lib_func.php"); ?>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <title>GIS Application - Extended Template by Cyber313</title>
        <meta name="author" content="luckynvic@gmail.com">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <link href="ext/customScroll/css/jquery.mCustomScrollbar.css" rel="stylesheet">
        <link href="css/style.default.css" rel="stylesheet">
        <link href="css/data_table.css" rel="stylesheet">
        <script src="../js/jquery-1.10.2.min.js"></script>
        <script src="js/data_table.js"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=3&amp;sensor=true&amp;key=AIzaSyABAiRMExl_KVCugrFbUO5FJwNTo_94vt0" type="text/javascript"></script>    

</head>

<body onLoad="load()" onUnload="GUnload()">

        <nav class="navbar navbar-default nav-fixed-top" role="navigation" id="app-nav-bar" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <?php judul(); ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <?php menuadmin(); ?>
            </div><!-- /.navbar-collapse -->
        </nav>

        <div id="map-canvas" ></div>
        <div class="visible-lg visible-md">
            <?php submenu(); ?>
        </div>
        
       
        
        
        <!-- form -->
        
  <div style="position: absolute;  left: 290px;  top: 98px; z-index: 3;" >
    <div id="map"></div>  
        <h3 align="center"><b>DATA KELURAHAN</b></h3><br />
        <button type="button" style="margin-bottom: 20px;" onclick="tampil_form()" class="btn btn-default">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Kelurahan
        </button>
        
        <!-- FORM -->
        <div id="form-input" style="margin-bottom: 50px; display: none;">
            <center>
            <h2>Tambah kelurahan</h2>
            <form method="post" action="<?=$_SERVER['PHP_SELF']?>">
                <input type="hidden" name="id" id="id_kelurahan" />
                <table border=0>
                    <tr>
                        <td>ID Kecamatan : </td>
                        <td><input type="text" name="id_kecamatan" id="id_kecamatan" /></td>
                    </tr>
                    <tr>
                        <td>Kelurahan : </td>
                        <td><input type="text" name="kelurahan" id="kelurahan" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             <input style="float: right; margin-top: 20px; margin-left: 10px;" onclick="hide_form()" type="reset" value="Reset" /> 
                             <input style="float: right; margin-top: 20px;" name="simpan" type="submit" value="Simpan" />   
                        </td>
                    </tr>
                </table>
            </form>
            </center>
            
            <?php
                if(isset($_POST['simpan']))
                {
                    $kecamatan = $_POST['id_kecamatan'];
                    $kelurahan = $_POST['kelurahan'];
                    
                    
                    $conn = koneksi();
                    $sql = "
                        INSERT INTO m_kelurahan (`id_kecamatan`, `kelurahan`) values
                        ('".$kecamatan."','".$kelurahan."')
                    ";
                    //var_dump($sql);
                    $query = mysql_query($sql);
                    
                    if($query) {
                        echo "<script>
                                alert ('Data Berhasil Disimpan');
                              </script>";
                        
                    } else {
                        echo "<script>
                                alert ('Data Gagal Disimpan');
                              </script>";
                    }
                }  
            ?>
        </div>
        <?php 
        $link=koneksi();
        //var_dump($link);
        $sql = "select * from m_kelurahan"; 
        $res = mysql_query($sql); 
        
        //var_dump($res);
        $banyakrecord=mysql_num_rows($res); 
        if($banyakrecord>0){ ?>
        <table id="example" class="display" style="font-size: medium; margin-left: 100px;">
            <thead>
							<tr>
								<th class="judultable">ID Kelurahan</th>
								<th class="judultable">ID Kecamatan</th>
								<th class="judultable">Nama</th>
                                <th class="judultable">Action</th>
							</tr>
            </thead>
            <tbody>
      <?php
        $i=0;                      
        while($data=mysql_fetch_array($res)){ $i++; 
        ?>   
        <tr>
            <td> <?php echo $data['id'];?> </td>
            <td> <?php echo $data['id_kecamatan'];?> </td>
            <td> <?php echo $data['kelurahan'];?> </td>
            <td><a href="kelurahan_form_edit.php?id=<?php echo $data['id'];?>"> <img alt="Hapus" style="width: 30px; height: 30px;" src="img/edit.png" /> </a>| 
            <a href="#" onclick="hapuskel('<?php echo $data['id'] ?> ')"><img alt="Hapus" style="width: 30px; height: 30px;" src="img/delete.png" /></a></td>
        </tr>                 
        <?php
        }
        ?>
        </tbody>
        </table>
        <?php
        }
        else {
            ?>
            Data Merk Tidak Ditemukan.
            <?php
        }
        ?>
       
    </div>
  
  <script>
  
  $(document).ready(function() {
    $('#example').DataTable();
    });

  /**MENU **/
    $(document).ready(function(){
        var menu_ul = $('.menu > li > ul'),
               menu_a  = $('.menu > li > a');
       
        menu_ul.hide();
   
        menu_a.click(function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                menu_a.removeClass('active');
                menu_ul.filter(':visible').slideUp('normal');
                $(this).addClass('active').next().stop(true,true).slideDown('normal');
            } else {
                $(this).removeClass('active');
                $(this).next().stop(true,true).slideUp('normal');
            }
        });
    });
   
function hapuskel(id) {
    var table='m_kelurahan';
    var data = 'id='+id+'&table='+table;
    $.ajax({
  url: "hapus.php",
  data: data,
  method: 'POST',
  success: function(result) {
   if (result=='sukses') {
    alert ('data dihapus');
    window.location = '<?php echo $_SERVER['PHP_SELF']?>';
   } else {
    alert ('gagal dihapus');
   }
  }
  
});//.done(function(data) {
  //console.log(data);
//});

}          


//slide ke bawah
function tampil_form()
{
    $("#form-input").slideDown();
}

function hide_form()
{
    $("#form-input").slideUp();
}
//----end slide ke bawah  
  </script>
</html>