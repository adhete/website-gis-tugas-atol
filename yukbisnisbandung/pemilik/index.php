<?php
   session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <title>GIS Application - Extended Template by Cyber313</title>
        <meta name="author" content="luckynvic@gmail.com">
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        
        <link href="ext/customScroll/css/jquery.mCustomScrollbar.css" rel="stylesheet">
        <link href="css/style.default.css" rel="stylesheet">
        <script src="../js/jquery-1.10.2.min.js"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=3&amp;sensor=true&amp;key=AIzaSyABAiRMExl_KVCugrFbUO5FJwNTo_94vt0" type="text/javascript"></script>    
</head>
<?php
 include("lib_func.php");
?>
      
    <body onLoad="load()" onUnload="GUnload()">

        <nav style="background-color: #b0aac2;" class="navbar navbar-default nav-fixed-top" role="navigation" id="app-nav-bar" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <?php judul(); ?>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <?php menuadmin(); ?>
            </div><!-- /.navbar-collapse -->
        </nav>

        <div id="map-canvas" ></div>
        <div class="visible-lg visible-md">
            <?php submenu(); ?>
        </div>
         <div style="margin-left:600px; margin-top:200px"> <h1> Selamat Datang :-) </h1> </div>      
        
   </body>
  
  <script>
  /**MENU **/
    $(document).ready(function(){
        var menu_ul = $('.menu > li > ul'),
               menu_a  = $('.menu > li > a');
       
        menu_ul.hide();
   
        menu_a.click(function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                menu_a.removeClass('active');
                menu_ul.filter(':visible').slideUp('normal');
                $(this).addClass('active').next().stop(true,true).slideDown('normal');
            } else {
                $(this).removeClass('active');
                $(this).next().stop(true,true).slideUp('normal');
            }
        });
    });
   
        
  
  </script>
</html>
