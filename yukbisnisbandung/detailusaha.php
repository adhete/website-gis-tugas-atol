<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="shortcut icon" href="img/yb.png" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <title>POTENSI USAHA DIBANDUNG</title>
        <link href="css/tampilan.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/fileinput.css" rel="stylesheet">
        <link href="ext/customScroll/css/jquery.mCustomScrollbar.css" rel="stylesheet">
        <link href="css/style.default.css" rel="stylesheet">
<script src="http://maps.google.com/maps?file=api&amp;v=3&amp;sensor=true&amp;key=AIzaSyABAiRMExl_KVCugrFbUO5FJwNTo_94vt0" type="text/javascript"></script>    
</head>

<?php
 include("koneksi/koneksi.php");
?>
      
    <body onUnload="GUnload()">

      <nav style="background-color: #b0aac2;" class="navbar navbar-default nav-fixed-top" role="navigation" id="app-nav-bar" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             </button>
       <a class="navbar-brand" href="index.php">YUK BISNIS</a> 
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav">
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                  

                </ul>
             <ul class="nav navbar-nav navbar-right">
                    <li><p class="navbar-text">Daftarkan Usaha Anda !!</p></li>
                    <li>
                        <a href="daftar.php">Daftar </a>
                    </li>
                    <li>
                        <a href="flogin.php">Login</a>
                    </li>

            </ul>
        </nav>

        <div id="map-canvas" ></div>
        <div class="visible-lg visible-md">

        <?php
            
          $id=$_GET['id'];
  
			  $con= koneksi_db();
			  $sql= "SELECT
						usaha.*,
					    m_kecamatan.kecamatan,
					    m_kelurahan.kelurahan,
					    skala.tipeskala,
					    sektor.NamaSektor
					FROM
						usaha
					    INNER JOIN m_kecamatan ON m_kecamatan.id = usaha.IDkecamatan
					    INNER JOIN m_kelurahan ON m_kelurahan.id = usaha.IDkelurahan
					    INNER JOIN skala ON skala.IDskala = usaha.IDskala
					    INNER JOIN sektor ON sektor.IDsektor = usaha.IDsektor 
					WHERE IDusaha='".$id."'";
			  $result = mysql_query($sql);
			  $row = mysql_fetch_assoc($result);

			  $sqlgbr = "SELECT * FROM gambar WHERE IDusaha='".$id."'";
         	  $resultgbr = mysql_query($sqlgbr);
         ?>
         
         <h3 style="text-align: center; font-weight: bold;"><?=$row['namausaha']; ?></h3><br />
         <div style="width: 800px; margin-left: 230px;" class="row">
           
            <div class="col-md-6">
            <div style="font-family:Calisto MT; font-size: 20px;">Detail</div>
            <hr />
             <table style="font-family: Humanst521 BT;
                    font-size: 14px;" 
                    border="0" width="350px">
                    
                
                
                <tr>
                    <td style="font-weight: bold;">Pemilik </td> 
                    <td><?=$row['pemilik']; ?></td>
                </tr>
              
                <tr>
                    <td style="font-weight: bold;">Produk Utama </td>
                    <td><?=$row['produkutama']; ?> </td>
                </tr>
                
                <tr>
                    <td style="font-weight: bold;">Alamat Usaha </td> 
                    <td><?=$row['alamat']; ?> </td>
                </tr>
                
                <tr>
                    <td style="font-weight: bold;">Kelurahan </td>
                    <td><?=$row['kelurahan']; ?> </td>
                </tr>
                
                <tr>
                    <td style="font-weight: bold;">Kecamatan </td>
                    <td><?=$row['kecamatan']; ?> </td>
                </tr>
                
                <tr>
                    <td style="font-weight: bold;">NO. Telepon </td>
                    <td><?=$row['telp']; ?> </td>
                </tr>
                
                <tr>
                    <td style="font-weight: bold;">Skala Usaha </td>
                    <td><?=$row['tipeskala']; ?> </td>
                </tr>
                
                <tr>
                    <td style="font-weight: bold;">Sektor Usaha </td>
                    <td><?=$row['NamaSektor']; ?> </td>
                </tr>
             </table><br />
             
             <div style="font-family:Calisto MT; font-size: 20px;">Deskripsi</div>
             <hr />
             <?=$row['deskripsi']; ?>
            </div>
            
             <div class="col-md-6">
              <div style="font-family:Calisto MT; font-size: 20px;">Gambar Usaha</div>
             <hr />
              
	             <?php
	                while ( $row = mysql_fetch_assoc($resultgbr)) {
	             ?>
	                  <img style="width: 250px; height: 250px;" src="<?=$row['gambar']; ?>"> 
	             <?php
	                }
	             ?>
	               
             
             </div>
           
           </div>

        </div>        
        <br />
        <div class="footer">
    <p>Copyright &copy; 2016 YukBisnis All Rights Reserved.</p>
</div>

        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="ext/customScroll/js/jquery.mCustomScrollbar.min.js"></script>
        <script src="ext/customScroll/js/jquery.mousewheel.min.js"></script>
        <script src="js/application.js"></script>
        <script src="js/fileinput.js"></script>
        <script type="text/javascript"> </script>
 	
    </body>
</html>
