-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Agu 2016 pada 10.54
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maps`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `IDadmin` varchar(20) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `adminuser` varchar(20) NOT NULL,
  `adminpass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`IDadmin`, `nama`, `adminuser`, `adminpass`) VALUES
('1', 'admin', 'admin', '*01A6717B58FF5C7EAFFF6CB7C96F7428EA65FE4C');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE `gambar` (
  `IDgambar` int(5) NOT NULL,
  `IDusaha` int(5) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gambar`
--

INSERT INTO `gambar` (`IDgambar`, `IDusaha`, `gambar`) VALUES
(17, 38, '../gambar/usaha-14-mie-ayam-kampung.jpg'),
(18, 39, '../gambar/usaha-jaket kulit ariel noah.png'),
(19, 40, '../gambar/usaha-14-mie-ayam-kampung.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `koordinat`
--

CREATE TABLE `koordinat` (
  `tempat` varchar(20) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `long` float(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `koordinat`
--

INSERT INTO `koordinat` (`tempat`, `lat`, `long`) VALUES
('lembang', -6.818081, 107.608635),
('coblong', -6.881724, 107.598389);

-- --------------------------------------------------------

--
-- Struktur dari tabel `markers`
--

CREATE TABLE `markers` (
  `id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `alamat` varchar(80) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `tipe` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `markers`
--

INSERT INTO `markers` (`id`, `nama`, `alamat`, `lat`, `lng`, `tipe`) VALUES
(1, 'Museum Benteng Vredeburg', 'Jl. Jend. A. Yani No.6 , Yogyakarta', -7.800330, 110.365967, 'museum'),
(2, 'Museum Biologi UGM', 'Jl. Sultan Agung No. 22, Yogyakarta.', -7.801969, 110.374374, 'museum'),
(3, 'Museum Affandi', 'Jl. Laksda Adisutjipto 167 Yogyakarta', -7.783037, 110.396164, 'museum'),
(4, 'Museum Wayang Kekayon', 'Jl. Raya Yogya-Wonosari Km. 7, Kec. Banguntapan, Bantul', -7.814852, 110.413033, 'museum'),
(5, 'Gembira Loka', 'Jl. Kebun Raya no. 2 Yogyakarta.', -7.803342, -110.397629, 'wisata'),
(6, 'Tugu Yogyakarta', 'Perempatan Jl. Pangeran Mangkubumi', -7.782958, 110.367043, 'wisata'),
(7, 'Monumen Jogja Kembali', 'Jl. Lingkar Utara, Jongkang, Sariharjo, Ngaglik, Sleman, YK', -7.749448, 110.369568, 'wisata'),
(8, 'Kraton Yogyakarta', 'Alun-alun utara, yogyakarta', -7.805708, 110.364113, 'wisata'),
(9, 'Malioboro', 'Jl. Malioboro, Yogyakarta', -7.793238, 110.365723, 'wisata');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kecamatan`
--

CREATE TABLE `m_kecamatan` (
  `id` smallint(2) NOT NULL,
  `kecamatan` varchar(30) DEFAULT NULL,
  `lat` varchar(20) NOT NULL,
  `long` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_kecamatan`
--

INSERT INTO `m_kecamatan` (`id`, `kecamatan`, `lat`, `long`) VALUES
(1, 'SUKASARI', '-6.8646647\n', '107.5889804\n'),
(2, 'COBLONG', '-6.8919604\n', '107.6156133\n'),
(3, 'BABAKAN CIPARAY', '-6.942333\n', '107.5771444\n'),
(4, 'BOJONGLOA KALER', '-6.9174639\n', '107.6191228\n'),
(5, 'ANDIR', '-6.9114323\n', '107.5771444\n'),
(6, 'CICENDO', '-6.9174639\n', '107.6191228\n'),
(7, 'SUKAJADI', '-6.9174639\n', '107.6191228\n'),
(8, 'CIDADAP', '-6.8639965\n', '107.6067354\n'),
(9, 'BANDUNG WETAN', '-6.9047153\n', '107.6185727\n'),
(10, 'ASTANA ANYAR', '-6.9217406\n', '107.6965284\n'),
(11, 'REGOL', '-6.940982\n', '107.612654\n'),
(12, 'BATUNUNGGAL', '-6.9194842\n', '107.6363296\n'),
(13, 'LENGKONG', '-6.9326938\n', '107.627451\n'),
(14, 'CIBEUNYING KIDUL', '-6.8984464\n', '107.6481682\n '),
(15, 'BANDUNG KULON', '-6.9376312\n', '107.5653089\n'),
(16, 'KIARACONDONG', '-6.9174639\n', '107.6191228\n'),
(17, 'BOJONGLOA KIDUL', '-6.9519605\n', '107.5948986\n'),
(18, 'CIBEUNYING KALER', '-6.9174639\n', '107.6191228\n'),
(19, 'SUMUR BANDUNG', '-6.9174639\n', '107.6191228\n'),
(20, 'CICADAS', '0.000000', '0.000000'),
(21, 'BANDUNG KIDUL', '-6.9557571\n', '107.6304105\n'),
(22, 'MARGACINTA', '0.000000', '0.000000'),
(23, 'RANCASARI', '-6.9539456\n', '107.6777669\n'),
(24, 'ARCAMANIK', '-6.913147\n', '107.692415\n'),
(25, 'CIBIRU', '-6.9163257\n', '107.7192104\n'),
(26, 'UJUNGBERUNG', '-6.9064866\n', '107.7073688\n'),
(27, 'ANTAPANI', '-6.9185834\n', '107.6600073\n'),
(28, 'BUAHBATU', '-6.9523899\n', '107.6511279\n'),
(29, 'CINAMBO', '-6.9328971\n', '107.6896073\n'),
(30, 'GEDEBAGE', '-6.9505768\n', '107.6984879\n'),
(31, 'MANDALAJATI', '-6.9174639\n', '107.6191228\n'),
(32, 'PANYILEUKAN', '-6.932218\n', '107.7073688\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kelurahan`
--

CREATE TABLE `m_kelurahan` (
  `id` smallint(3) NOT NULL,
  `id_kecamatan` smallint(2) DEFAULT NULL,
  `kelurahan` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_kelurahan`
--

INSERT INTO `m_kelurahan` (`id`, `id_kecamatan`, `kelurahan`) VALUES
(1, 1, 'SUKASARA'),
(2, 1, 'GEGERKALONG'),
(3, 1, 'ISOLA'),
(4, 1, 'SARIJADI'),
(5, 2, 'CIPAGANTI'),
(6, 2, 'LEBAKGEDE'),
(7, 2, 'SADANGSERANG'),
(8, 2, 'DAGO'),
(9, 2, 'SEKELOA'),
(10, 2, 'LEBAK SILIWANGI'),
(11, 3, 'BABAKAN CIPARAY'),
(12, 3, 'BABAKAN'),
(13, 3, 'SUKAHAJI'),
(14, 3, 'MARGAHAYU UTARA'),
(15, 3, 'MARGASUKA'),
(16, 3, 'CIRANGRANG'),
(17, 4, 'KOPO'),
(18, 4, 'BABAKAN TAROGONG'),
(19, 4, 'JAMIKA'),
(20, 4, 'BABAKAN ASIH'),
(21, 4, 'SUKA ASIH'),
(22, 5, 'MALEER'),
(23, 5, 'DUNGUS CARIANG'),
(24, 5, 'CIROYOM'),
(25, 5, 'KEBONJERUK'),
(26, 5, 'GARUDA'),
(27, 5, 'CAMPAKA'),
(28, 6, 'HUSEN SASTRANEGARA'),
(29, 6, 'ARJUNA'),
(30, 6, 'PAJAJARAN'),
(31, 6, 'PASIR KALIKI'),
(32, 6, 'PAMOYANAN'),
(33, 6, 'SUKARAJA'),
(34, 7, 'PASTEUR'),
(35, 7, 'CIPEDES'),
(36, 7, 'SUKAWARNA'),
(37, 7, 'SUKAGALIH'),
(38, 7, 'SUKABUNGAH'),
(39, 8, 'HEGARMANAH'),
(40, 8, 'CIUMBULEUIT'),
(41, 8, 'LEDENG'),
(42, 9, 'CIHAPIT'),
(43, 9, 'TAMANSARI'),
(44, 9, 'CITARUM'),
(45, 10, 'KARASAK'),
(46, 10, 'NYENGSERET'),
(47, 10, 'KARANGANYAR'),
(48, 10, 'PANJUNAN'),
(49, 10, 'CIBADAK'),
(50, 10, 'PELINDUNG HEWAN'),
(51, 11, 'CIGERELENG'),
(52, 11, 'ANCOL'),
(53, 11, 'PUNGKUR'),
(54, 11, 'BALONGGEDE'),
(55, 11, 'CIREUREUP'),
(56, 11, 'CIATEUL'),
(57, 11, 'PASIRLUYU'),
(58, 12, 'GUMURUH'),
(59, 12, 'MALEER'),
(60, 12, 'CIBANGKONG'),
(61, 12, 'KACAPIRING'),
(62, 12, 'KEBONWARU'),
(63, 12, 'KEBONGEDANG'),
(64, 12, 'SAMOJA'),
(65, 12, 'BINONG'),
(66, 13, 'CIJAGRA'),
(67, 13, 'LINGKAR SELATAN'),
(68, 13, 'BURANGRANG'),
(69, 13, 'PALEDANG'),
(70, 13, 'TURANGGA'),
(71, 13, 'MALABAR'),
(72, 13, 'CIKAWAO'),
(73, 14, 'PADASUKA'),
(74, 14, 'CIKUTRA'),
(75, 14, 'CICADAS'),
(76, 14, 'SUKAMAJU'),
(77, 14, 'SUKAPADA'),
(78, 14, 'PASIRLAYUNG'),
(79, 15, 'CIJERAH'),
(80, 15, 'CIBUNTU'),
(81, 15, 'WARUNG MUNCANG'),
(82, 15, 'CARINGIN'),
(83, 15, 'CIGONDEWAH KALER'),
(84, 15, 'GEMPOLSARI'),
(85, 15, 'CIGONDEWAH RAHAYU'),
(86, 15, 'CIGONDEWAH KIDUL'),
(87, 16, 'SUKAPURA'),
(88, 16, 'KEBON JAYANTI'),
(89, 16, 'BABAKAN SURABAYA'),
(90, 16, 'CICAHEUM'),
(91, 16, 'BABAKANSARI'),
(92, 16, 'KEBON KANGKUNG'),
(93, 17, 'SITUSAEUR'),
(94, 17, 'KEBON LEGA'),
(95, 17, 'CIBADUYUT'),
(96, 17, 'MEKARWANGI'),
(97, 17, 'CIBADUYUT KIDUL'),
(98, 17, 'CIBADUYUT WETAN'),
(99, 18, 'CIHAUR GEULIS'),
(100, 18, 'SUKALUYU'),
(101, 18, 'NEGLASARI'),
(102, 18, 'CIGADUNG'),
(103, 19, 'BRAGA'),
(104, 19, 'MERDEKA'),
(105, 19, 'KEBON PISANG'),
(106, 19, 'BABAKAN CIAMIS'),
(107, 20, 'ANTAPANI'),
(108, 20, 'MANDALAJATI'),
(109, 20, 'KARANGPAMULANG'),
(110, 20, 'ANTAPANI TENGAH'),
(111, 20, 'ANTAPANI KIDUL'),
(112, 21, 'BATUNUNGGAL'),
(113, 21, 'WATES'),
(114, 21, 'MENGER'),
(115, 21, 'KUJANGSARI'),
(116, 22, 'SEKEJATI'),
(117, 22, 'MARGASARI'),
(118, 22, 'MARGASENANG'),
(119, 23, 'CIPAMOKOLAN'),
(120, 23, 'DARWATI'),
(121, 23, 'CISARANTEN KIDUL'),
(122, 23, 'MEKARMULYA'),
(123, 24, 'SUKAMISKIN'),
(124, 24, 'CISARANTEN BINA HARAPAN'),
(125, 24, 'CISARANTEN KULON'),
(126, 24, 'SINDANGLAYA'),
(127, 25, 'PALASARI'),
(128, 25, 'CIPADUNG'),
(129, 25, 'PASIRBIRU'),
(130, 25, 'CISURUPAN'),
(131, 25, 'CIPADUNG KULON'),
(132, 25, 'CIPADUNG KIDUL'),
(133, 26, 'UJUNGBERUNG'),
(134, 26, 'CISARANTEN WETAN'),
(135, 26, 'PASIR ENDAH'),
(136, 26, 'CIGENDING'),
(137, 26, 'PASIRWENGI'),
(138, 26, 'PASIRJATI'),
(139, 26, 'PASANGGRAHAN'),
(140, 27, 'ANTAPANI KIDUL'),
(141, 27, 'ANTAPANI KULON'),
(142, 27, 'ANTAPANI TENGAH'),
(143, 27, 'ANTAPANI WETAN'),
(144, 28, 'CIJAWURA'),
(145, 28, 'JATISARI'),
(146, 28, 'MARGASARI'),
(147, 28, 'SEKEJATI'),
(148, 29, 'BABAKAN PENGHULU'),
(149, 29, 'CISARANTEN WETAN'),
(150, 29, 'PAKEMITAN'),
(151, 29, 'SUKAMULYA'),
(152, 30, 'CIMINCRANG '),
(153, 30, 'CISARANTEN'),
(154, 30, 'RANCABOLANG'),
(155, 30, 'RANCANUMPANG'),
(156, 31, 'JATIHANDAP'),
(157, 31, 'KARANGPAMULANG'),
(158, 31, 'JATIHANDAP'),
(159, 31, 'SINDANGJAYA'),
(160, 32, 'CIPADUNG KIDUL'),
(161, 32, 'CIPADUNG KULON '),
(162, 32, 'CIPADUNG WETAN'),
(163, 32, 'MEKARMULYA');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilik`
--

CREATE TABLE `pemilik` (
  `namapemilik` varchar(20) NOT NULL,
  `Noktp` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `Tmplahir` varchar(15) NOT NULL,
  `tgllahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `jk` varchar(1) NOT NULL,
  `ktp` varchar(50) NOT NULL,
  `statusemail` char(1) NOT NULL,
  `statusaktif` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemilik`
--

INSERT INTO `pemilik` (`namapemilik`, `Noktp`, `password`, `Tmplahir`, `tgllahir`, `email`, `alamat`, `jk`, `ktp`, `statusemail`, `statusaktif`) VALUES
('as', '12345', 'bagongsumatra', 'krw', '2016-10-10', 'adityacharnita@gmail.com', 'asas', 'l', 'gambar/ktp-Capture.PNG', 'T', 'Y'),
('agung', '3273252612940001', 'adhete', 'karawang', '1995-03-10', 'adityacharnita@gmail.com', 'sekeloa utara no 59', 'l', 'gambar/ktp-Capture.PNG', 'Y', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sektor`
--

CREATE TABLE `sektor` (
  `IDsektor` int(5) NOT NULL,
  `NamaSektor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sektor`
--

INSERT INTO `sektor` (`IDsektor`, `NamaSektor`) VALUES
(1, 'Makanan'),
(2, 'Fashion'),
(3, 'Elektronik'),
(4, 'Komputer'),
(5, 'Smartphone'),
(6, 'Furniture'),
(7, 'Otomotif'),
(8, 'Mainan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `skala`
--

CREATE TABLE `skala` (
  `IDskala` int(5) NOT NULL,
  `tipeskala` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `skala`
--

INSERT INTO `skala` (`IDskala`, `tipeskala`) VALUES
(1, 'Mikro'),
(2, 'Kecil'),
(3, 'Menengah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `usaha`
--

CREATE TABLE `usaha` (
  `namausaha` varchar(50) NOT NULL,
  `pemilik` varchar(20) NOT NULL,
  `produkutama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `IDkelurahan` smallint(3) NOT NULL,
  `IDkecamatan` smallint(2) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `long` float(10,6) NOT NULL,
  `IDskala` int(5) NOT NULL,
  `IDsektor` int(5) NOT NULL,
  `IDusaha` int(5) NOT NULL,
  `Noktp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usaha`
--

INSERT INTO `usaha` (`namausaha`, `pemilik`, `produkutama`, `alamat`, `IDkelurahan`, `IDkecamatan`, `telp`, `deskripsi`, `lat`, `long`, `IDskala`, `IDsektor`, `IDusaha`, `Noktp`) VALUES
('Agung Mie Ayam', 'agung', 'Mie Ayam', 'Jalan Ahmad Yani No 100', 2, 1, '082295206322', 'Ngeunah Tumaninah ', -6.911920, 107.641953, 3, 1, 38, '3273252612940001'),
('Jaket Kulit Haneut', 'agung', 'Jaket Kulit', 'Jalan Jakarta No 107', 81, 15, '082295206322', 'Nyaman di pakai saat kapanpun', -6.914263, 107.639374, 3, 2, 39, '3273252612940001'),
('Mie', 'as', 'Mie baso', 'jakarta', 18, 4, '082295206387', 'sdhshd', -6.915243, 107.642761, 2, 1, 40, '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`IDadmin`);

--
-- Indexes for table `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`IDgambar`);

--
-- Indexes for table `markers`
--
ALTER TABLE `markers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_kelurahan`
--
ALTER TABLE `m_kelurahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kecamatan` (`id_kecamatan`);

--
-- Indexes for table `pemilik`
--
ALTER TABLE `pemilik`
  ADD PRIMARY KEY (`Noktp`);

--
-- Indexes for table `sektor`
--
ALTER TABLE `sektor`
  ADD PRIMARY KEY (`IDsektor`);

--
-- Indexes for table `skala`
--
ALTER TABLE `skala`
  ADD PRIMARY KEY (`IDskala`);

--
-- Indexes for table `usaha`
--
ALTER TABLE `usaha`
  ADD PRIMARY KEY (`IDusaha`),
  ADD KEY `Noktp` (`Noktp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gambar`
--
ALTER TABLE `gambar`
  MODIFY `IDgambar` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `markers`
--
ALTER TABLE `markers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `m_kecamatan`
--
ALTER TABLE `m_kecamatan`
  MODIFY `id` smallint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `m_kelurahan`
--
ALTER TABLE `m_kelurahan`
  MODIFY `id` smallint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `usaha`
--
ALTER TABLE `usaha`
  MODIFY `IDusaha` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `m_kelurahan`
--
ALTER TABLE `m_kelurahan`
  ADD CONSTRAINT `m_kelurahan_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `m_kecamatan` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `usaha`
--
ALTER TABLE `usaha`
  ADD CONSTRAINT `usaha_ibfk_1` FOREIGN KEY (`Noktp`) REFERENCES `pemilik` (`Noktp`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
